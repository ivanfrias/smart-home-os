package pt.ipl.estg.requests;

import java.io.IOException;
import java.net.ServerSocket;

public class EntryPoint {
	
	private static final int PORT_NUMBER = 1024;
	
	public void start(){
		startSocketComm();
	}
	
	public void startSocketComm(){
        boolean listening = true;     
        try (ServerSocket serverSocket = new ServerSocket(PORT_NUMBER)) { 
            while (listening) {
                new SocketServerThread(serverSocket.accept()).start();
            }
        } catch (IOException e) {
            System.err.println("Could not listen on port " + PORT_NUMBER);
            System.exit(-1);
        }
	}
}
	