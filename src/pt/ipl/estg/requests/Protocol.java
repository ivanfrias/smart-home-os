package pt.ipl.estg.requests;

import java.util.Arrays;

import pt.ipl.estg.devices.DeviceController;
import pt.ipl.estg.model.Command;

public class Protocol {
	DeviceController controllerPoint; 
	
	public Protocol(){
		controllerPoint = new DeviceController();
	}
	
	public String processInput(String inputLine) {
        String theOutput = null;
    	
    	Object[] parameters = Command.parseCommand(inputLine);
    	
    	if(parameters[0] != null && parameters[1] != null){
            switch ((Command)parameters[0]) {
    		case TURN_ON:
    			//controllerPoint.connectDevice(parameters); 
    			break;
    		case TURN_OFF:
    			//controllerPoint.disconnectDevice(parameters);
    			break;
    		case LIST_DEVICES:
    			theOutput = Arrays.toString(controllerPoint.getHomeDevices().toArray());
    			break;
    		}
    	}
    	
        return theOutput;
    }
}
