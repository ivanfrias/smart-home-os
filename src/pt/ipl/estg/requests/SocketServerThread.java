package pt.ipl.estg.requests;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

import org.apache.commons.lang3.StringUtils;

public class SocketServerThread extends Thread {
	private Socket socket;
	
	public SocketServerThread(Socket socket){
		this.socket = socket;
	}
	
	public void run(){
        try { 
            PrintWriter out =
                new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(
                new InputStreamReader(socket.getInputStream()));
         
            String inputLine, outputLine;
             
            Protocol protocol = new Protocol();
 
            while ((inputLine = in.readLine()) != null) {
            	System.out.println("Read Line " + inputLine);
            	
                outputLine = protocol.processInput(inputLine);
                out.println(outputLine);
                if (StringUtils.equalsIgnoreCase(outputLine,"quit")){
                    break;
                }
            }
            socket.close();
            
        } catch (IOException e) {
        	e.printStackTrace();
        }
	}
}
