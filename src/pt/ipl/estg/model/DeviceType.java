package pt.ipl.estg.model;


public class DeviceType {
	private String description;
	private Command[] allowedCommands;
	
	public DeviceType(String description, Command[] allowedCommands){
		this.description = description;
		this.allowedCommands = allowedCommands;
	}

	public String getDescription() {
		return description;
	}

	public Command[] getAllowedCommands() {
		return allowedCommands;
	}
}
