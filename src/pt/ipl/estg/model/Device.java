package pt.ipl.estg.model;

public class Device {
	
	private DeviceType deviceType;
	
	private short vendor;
	private short product;
	private byte serialNumber;
	private int address;
	
	public Device(short idVendor, short idProduct, byte iSerialNumber,
			int address) {
		this.vendor = idVendor;
		this.product = idProduct;
		this.serialNumber = iSerialNumber;
		this.address = address;
	}

	public short getVendor() {
		return vendor;
	}
	public short getProduct() {
		return product;
	}
	public byte getSerialNumber() {
		return serialNumber;
	}
	public int getAddress() {
		return address;
	}

	public DeviceType getDeviceType() {
		return deviceType;
	}

	public void setDeviceType(DeviceType deviceType) {
		this.deviceType = deviceType;
	}
}
