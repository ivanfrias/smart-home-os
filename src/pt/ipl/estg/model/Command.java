package pt.ipl.estg.model;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

public enum Command {
	TURN_ON("turn_on"),
	TURN_OFF("turn_off"),
	LIST_DEVICES("list_devices");
	
	private String action;
	
	private Command(String actionToPerform){
		this.action = actionToPerform;
	}

	public String getAction() {
		return action;
	}
	
	public static Object[] parseCommand(String line){
		Object[] parameters = new Object[2];
		
		if(StringUtils.isNotEmpty(line)){
			String[] splitParams = StringUtils.split(line, " ");
				if(StringUtils.equalsIgnoreCase(splitParams[0], TURN_ON.getAction())){
					parameters[0] = TURN_ON;
					parameters[1] = ArrayUtils.subarray(splitParams, 1, splitParams.length); 
				} else if (StringUtils.equalsIgnoreCase(splitParams[0], TURN_OFF.getAction())){
					parameters[0] = TURN_OFF;
					parameters[1] = ArrayUtils.subarray(splitParams, 1, splitParams.length); 
				} else if (StringUtils.equalsIgnoreCase(splitParams[0], LIST_DEVICES.getAction())){
					parameters[0] = LIST_DEVICES;
					parameters[1] = ArrayUtils.subarray(splitParams, 1, splitParams.length);
				}
		}
		
		return parameters; 
	}
}
