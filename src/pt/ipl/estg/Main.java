package pt.ipl.estg;

import java.io.IOException;

import org.apache.log4j.Logger;

import pt.ipl.estg.devices.bluetooth.RemoteDeviceDiscovery;
import pt.ipl.estg.requests.EntryPoint;

/***
 * This classes is the entry point for the Importer Program
 * 
 * @author ivanfrias
 * 
 */
public class Main {

	private static final Logger log = Logger.getLogger(Main.class);
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		System.out.println("Before starting threads BT devices Search");
		RemoteDeviceDiscovery disovery = new RemoteDeviceDiscovery();
		Thread deviceThread = new Thread(disovery);
		deviceThread.start();
		
		System.out.println("Before starting threads");
		EntryPoint entryPoint = new EntryPoint();
		entryPoint.start();
		System.out.println("After starting threads");
	}

}