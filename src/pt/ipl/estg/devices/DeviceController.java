package pt.ipl.estg.devices;

import java.util.ArrayList;
import java.util.List;

import org.usb4java.Context;
import org.usb4java.Device;
import org.usb4java.DeviceDescriptor;
import org.usb4java.DeviceList;
import org.usb4java.LibUsb;
import org.usb4java.LibUsbException;

public class DeviceController {
	
	List<pt.ipl.estg.model.Device> devices;

	public DeviceController(){
		devices = getHomeDevices();
	}
	
	public List<pt.ipl.estg.model.Device> getConnectedDevices() {
		return queryUsbConnectedDevices();
	}

	public List<pt.ipl.estg.model.Device> queryUsbConnectedDevices() {
		List<pt.ipl.estg.model.Device> listOfDevices = new ArrayList<pt.ipl.estg.model.Device>();
		Context context = new Context();

		int result = LibUsb.init(context);
		if (result < 0) {
			throw new LibUsbException("Unable to initialize libusb", result);
		}

		DeviceList list = new DeviceList();
		result = LibUsb.getDeviceList(context, list);
		if (result < 0) {
			throw new LibUsbException("Unable to get device list", result);
		}

		try {
			// Iterate over all devices and list them
			for (Device device : list) {
				int address = LibUsb.getDeviceAddress(device);
				DeviceDescriptor descriptor = new DeviceDescriptor();
				result = LibUsb.getDeviceDescriptor(device, descriptor);
				if (result < 0) {
					throw new LibUsbException(
							"Unable to read device descriptor", result);
				}

				pt.ipl.estg.model.Device newDevice = new pt.ipl.estg.model.Device(
						descriptor.idVendor(), descriptor.idProduct(),
						descriptor.iSerialNumber(), address);
				listOfDevices.add(newDevice);
			}
		} finally {
			LibUsb.freeDeviceList(list, true);
		}

		LibUsb.exit(context);
		
		return listOfDevices;
	}
	
	public List<pt.ipl.estg.model.Device> getHomeDevices(){
		//assumes that all connected devices are home devices..	
		return getConnectedDevices();
	}
}
