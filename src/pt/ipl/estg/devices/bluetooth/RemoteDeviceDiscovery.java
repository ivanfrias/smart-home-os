package pt.ipl.estg.devices.bluetooth;

import java.io.IOException;
import java.util.Vector;

import javax.bluetooth.*;

public class RemoteDeviceDiscovery implements Runnable {

	/* <RemoteDevice> */
	private Vector devicesDiscovered = new Vector();
	final Object inquiryCompletedEvent = new Object();

	public void serachForDevices() throws IOException, InterruptedException {

		devicesDiscovered.clear();

		DiscoveryListener listener = new DiscoveryListener() {

			public void deviceDiscovered(RemoteDevice btDevice, DeviceClass cod) {
				try {
					System.out.println("Device " + btDevice.getBluetoothAddress()
							+ " found - "+"     name "
							+ btDevice.getFriendlyName(false));
					devicesDiscovered.addElement(btDevice);
				} catch (IOException cantGetDeviceName) {
				}
			}

			public void inquiryCompleted(int discType) {
				System.out.println("Device Inquiry completed!");
				synchronized (inquiryCompletedEvent) {
					inquiryCompletedEvent.notifyAll();
				}
			}

			public void serviceSearchCompleted(int transID, int respCode) {
			}

			public void servicesDiscovered(int transID,
					ServiceRecord[] servRecord) {
			}
		};

		synchronized (inquiryCompletedEvent) {
			boolean started = LocalDevice.getLocalDevice().getDiscoveryAgent()
					.startInquiry(DiscoveryAgent.GIAC, listener);
			if (started) {
				System.out.println("wait for device inquiry to complete...");
				inquiryCompletedEvent.wait();
				System.out.println(devicesDiscovered.size()
						+ " device(s) found");
			}
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			this.serachForDevices();
		} catch (IOException | InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
